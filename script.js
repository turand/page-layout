"use strict"

//toggles and removes the dropdown li element (Filter)

function myToggle() {
  const target = document.getElementById("mydropdown");
  if (target.classList.contains("dropdown-content")) {
      target.classList.remove("dropdown-content");
      return
  }
  target.classList.add("dropdown-content");
}

//targets and removes the class, which contains the popup animation

function addClass(ID) {
  const div = document.getElementById(ID);
  div.className = "pop-up";
}

function removeClass(ID) {
  const div = document.getElementById(ID);
  if (div.classList.contains("pop-up")) {
    div.classList.remove("pop-up");
  }
}

//change the value of the input element and it's background color

function change(elemId) {
    let elem = document.getElementById(elemId);
    if (elem.value === "+ Add To Cart") {
      elem.value = "- Remove Item";
      elem.style.backgroundColor = "gray";
    }
    else {
      elem.value = "+ Add To Cart";
      elem.style.backgroundColor = "#3853D8";
    }
}


